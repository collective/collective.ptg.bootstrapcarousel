Introduction
===============================

Basic integration of the twitter bootstrap carousel with collective.plonetruegallery
http://twitter.github.io/bootstrap/javascript.html#carousel

This add-on will only work with Plone >= 4.3  as Jquery 1.7 is not supported on previous versions of Plone. 

Bootstrap's carousel needs Jquery 1.7 or higher

